package com.cgs.lms.dto;
// Generated Jun 24, 2016 3:56:02 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * OrderItems generated by hbm2java
 */
public class OrderItemsDto implements java.io.Serializable {

	private int id;
	private Integer orderId;
	private Integer invMastUid;
	private String itemId;
	private String itemDesc;
	private Integer quantity;
	private String jobNumber;
	private Date createdAt;
	private Date updatedAt;
	private Integer lineNum;
	private String defaultSellingUnit;
	private String itemImage;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Integer getOrderId() {
		return orderId;
	}


	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}


	public Integer getInvMastUid() {
		return invMastUid;
	}


	public void setInvMastUid(Integer invMastUid) {
		this.invMastUid = invMastUid;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getItemDesc() {
		return itemDesc;
	}


	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public String getJobNumber() {
		return jobNumber;
	}


	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Integer getLineNum() {
		return lineNum;
	}


	public void setLineNum(Integer lineNum) {
		this.lineNum = lineNum;
	}


	public String getDefaultSellingUnit() {
		return defaultSellingUnit;
	}


	public void setDefaultSellingUnit(String defaultSellingUnit) {
		this.defaultSellingUnit = defaultSellingUnit;
	}


	public String getItemImage() {
		return itemImage;
	}


	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}


	public OrderItemsDto() {
	}



}
