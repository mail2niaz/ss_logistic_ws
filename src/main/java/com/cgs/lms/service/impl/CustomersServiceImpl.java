package com.cgs.lms.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cgs.lms.domain.repository.CustomersRepository;
import com.cgs.lms.dto.CustomersDto;
import com.cgs.lms.mapper.CustomersMapper;
import com.cgs.lms.service.CustomersService;

@Component("CustomersServiceImpl")
public class CustomersServiceImpl implements CustomersService {

	@Autowired
	CustomersRepository customersRepository;

	@Inject
	private CustomersMapper customersMapper;

	@Override
	public CustomersDto getCustomersDto(int customerId) {
		List<Object[]> objs = customersRepository.findCustomerIdById(customerId);
		Object[] objval = objs.get(0);

		CustomersDto customersDto = new CustomersDto();
		customersDto.setName(String.valueOf(objval[0]));
		customersDto.setAddress(String.valueOf(objval[1]));
		customersDto.setCity(String.valueOf(objval[2]));
		customersDto.setZipCode(String.valueOf(objval[3]));
		customersDto.setPhone(String.valueOf(objval[4]));
		return customersDto;
	}

}
