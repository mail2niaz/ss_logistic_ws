package com.cgs.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cgs.lms.domain.Orders;
import com.cgs.lms.dto.OrdersDto;

@Service
public interface OrderService {

	public List<OrdersDto> orders(int truckId, String status);

	public Orders updateOrder(int order_id, String order_status);

	public List<OrdersDto> orders(String orgUserId, String status);

	public Orders updateOrder(int parseInt, String order_status, int parseInt2);

}
