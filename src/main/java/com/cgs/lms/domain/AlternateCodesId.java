package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AlternateCodesId generated by hbm2java
 */
@Embeddable
public class AlternateCodesId implements java.io.Serializable {

	private Integer id;
	private String alternateCode;

	public AlternateCodesId() {
	}

	public AlternateCodesId(Integer id, String alternateCode) {
		this.id = id;
		this.alternateCode = alternateCode;
	}

	@Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "alternate_code")
	public String getAlternateCode() {
		return this.alternateCode;
	}

	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AlternateCodesId))
			return false;
		AlternateCodesId castOther = (AlternateCodesId) other;

		return ((this.getId() == castOther.getId())
				|| (this.getId() != null && castOther.getId() != null && this.getId().equals(castOther.getId())))
				&& ((this.getAlternateCode() == castOther.getAlternateCode())
						|| (this.getAlternateCode() != null && castOther.getAlternateCode() != null
								&& this.getAlternateCode().equals(castOther.getAlternateCode())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result + (getAlternateCode() == null ? 0 : this.getAlternateCode().hashCode());
		return result;
	}

}
