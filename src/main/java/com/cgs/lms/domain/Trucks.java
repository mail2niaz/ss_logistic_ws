package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Trucks generated by hbm2java
 */
@Entity
@Table(name = "trucks", schema = "public")
public class Trucks implements java.io.Serializable {

	private int id;
	private Integer userId;
	private String name;
	private Integer activeFlag;
	private Date createdAt;
	private Date updatedAt;
	private String password;

	public Trucks() {
	}

	public Trucks(int id) {
		this.id = id;
	}

	public Trucks(int id, Integer userId, String name, Integer activeFlag, Date createdAt, Date updatedAt,
			String password) {
		this.id = id;
		this.userId = userId;
		this.name = name;
		this.activeFlag = activeFlag;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.password = password;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "active_flag")
	public Integer getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", length = 29)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "password", length = 100)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
