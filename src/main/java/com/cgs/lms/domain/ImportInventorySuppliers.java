package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ImportInventorySuppliers generated by hbm2java
 */
@Entity
@Table(name = "import_inventory_suppliers", schema = "public")
public class ImportInventorySuppliers implements java.io.Serializable {

	private ImportInventorySuppliersId id;

	public ImportInventorySuppliers() {
	}

	public ImportInventorySuppliers(ImportInventorySuppliersId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "invMastUid", column = @Column(name = "inv_mast_uid")),
			@AttributeOverride(name = "supplierId", column = @Column(name = "supplier_id")),
			@AttributeOverride(name = "divisionId", column = @Column(name = "division_id")),
			@AttributeOverride(name = "leadTimeDays", column = @Column(name = "lead_time_days")),
			@AttributeOverride(name = "upcCode", column = @Column(name = "upc_code", length = 14)),
			@AttributeOverride(name = "checkDigit", column = @Column(name = "check_digit")),
			@AttributeOverride(name = "catalogName", column = @Column(name = "catalog_name", length = 40)),
			@AttributeOverride(name = "catalogPage", column = @Column(name = "catalog_page", length = 6)),
			@AttributeOverride(name = "msds", column = @Column(name = "msds", length = 12)),
			@AttributeOverride(name = "deleteFlag", column = @Column(name = "delete_flag")),
			@AttributeOverride(name = "dateCreated", column = @Column(name = "date_created", length = 29)),
			@AttributeOverride(name = "dateLastModified", column = @Column(name = "date_last_modified", length = 29)),
			@AttributeOverride(name = "lastMaintainedBy", column = @Column(name = "last_maintained_by", length = 30)),
			@AttributeOverride(name = "supplierPartNo", column = @Column(name = "supplier_part_no", length = 40)),
			@AttributeOverride(name = "supplierSortCode", column = @Column(name = "supplier_sort_code", length = 10)),
			@AttributeOverride(name = "listPrice", column = @Column(name = "list_price", scale = 9)),
			@AttributeOverride(name = "cost", column = @Column(name = "cost", scale = 9)),
			@AttributeOverride(name = "manufacturingClassId", column = @Column(name = "manufacturing_class_id")),
			@AttributeOverride(name = "backhaulAmount", column = @Column(name = "backhaul_amount")),
			@AttributeOverride(name = "backhaulType", column = @Column(name = "backhaul_type", length = 10)),
			@AttributeOverride(name = "inventorySupplierUid", column = @Column(name = "inventory_supplier_uid")),
			@AttributeOverride(name = "contractNumber", column = @Column(name = "contract_number", length = 40)),
			@AttributeOverride(name = "createdBy", column = @Column(name = "created_by")),
			@AttributeOverride(name = "supplierPurchaseUnit", column = @Column(name = "supplier_purchase_unit", length = 8)),
			@AttributeOverride(name = "parkerPartLength", column = @Column(name = "parker_part_length")),
			@AttributeOverride(name = "dateCostLastModified", column = @Column(name = "date_cost_last_modified")),
			@AttributeOverride(name = "upcCodeSourceTypeCd", column = @Column(name = "upc_code_source_type_cd")),
			@AttributeOverride(name = "futureCost", column = @Column(name = "future_cost")),
			@AttributeOverride(name = "effectiveDate", column = @Column(name = "effective_date")),
			@AttributeOverride(name = "freightFactor", column = @Column(name = "freight_factor", scale = 9)),
			@AttributeOverride(name = "futureListPrice", column = @Column(name = "future_list_price")),
			@AttributeOverride(name = "priceExpirationDate", column = @Column(name = "price_expiration_date")),
			@AttributeOverride(name = "priceCostLastUpdateDate", column = @Column(name = "price_cost_last_update_date")),
			@AttributeOverride(name = "costMultiplier", column = @Column(name = "cost_multiplier")) })
	public ImportInventorySuppliersId getId() {
		return this.id;
	}

	public void setId(ImportInventorySuppliersId id) {
		this.id = id;
	}

}
