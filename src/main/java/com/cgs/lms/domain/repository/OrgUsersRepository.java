package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrgUsers;

public interface OrgUsersRepository extends JpaRepository<OrgUsers, Integer> {

	List<OrgUsers> findAllByEmailAndPwd(String email, String password);

	OrgUsers findOneByEmail(String email);
}
