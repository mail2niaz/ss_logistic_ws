package com.cgs.lms.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.ItemImages;

public interface ItemImageRepository extends JpaRepository<ItemImages, Integer> {

	ItemImages findOneByItemId(String string);

}
