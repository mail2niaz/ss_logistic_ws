package com.cgs.lms.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.cgs.lms.domain.Users;

public interface UserRepository extends CrudRepository<Users, Integer>{
}
