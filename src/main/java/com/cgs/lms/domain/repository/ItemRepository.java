package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cgs.lms.domain.Items;
import com.cgs.lms.domain.ItemsId;

public interface ItemRepository extends JpaRepository<Items, ItemsId> {

	Iterable<Items> findAllById_Id(int id);

	@Query("select it.id.id, it.id.itemId,it.id.itemDesc from Items it")
	public Iterable<Object[]> findItemDesc();
	
	@Query(value = "select it.id.id, it.id.itemId,it.id.itemDesc from Items it", nativeQuery=false)
	public Iterable<Object[]> findItemDesc1();

	@Query("select it.id  from Items it")
	<Optional>List<ItemsId> findItemDesc2();

	// @Query(value = "select it from Items it", nativeQuery = true)
	// List<String[]> findAllItems();
	//
	// @Query("select it.id from Items it")
	// List<ItemsId> findAllItemIds();

}
