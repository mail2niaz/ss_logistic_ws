package com.cgs.lms.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.cgs.lms.domain.Contacts;

public interface ContactRepository extends CrudRepository<Contacts, Integer>{
}
