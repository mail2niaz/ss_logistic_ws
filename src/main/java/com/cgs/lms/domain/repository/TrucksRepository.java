package com.cgs.lms.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.Trucks;

public interface TrucksRepository extends JpaRepository<Trucks, Integer> {

	Trucks findOneByIdAndPassword(int truckId, String password);
}
