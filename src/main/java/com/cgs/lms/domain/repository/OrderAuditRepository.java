package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrderAudit;

public interface OrderAuditRepository extends JpaRepository<OrderAudit, Integer> {

	List<OrderAudit> findAllByOrderId(int order_id);

}
