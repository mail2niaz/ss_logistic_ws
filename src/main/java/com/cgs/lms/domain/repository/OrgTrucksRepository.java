package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrgTrucks;

public interface OrgTrucksRepository extends JpaRepository<OrgTrucks, Integer> {

	OrgTrucks findOneByName(String name);

	List<OrgTrucks> findAllByLocationId(int locationId);

}
