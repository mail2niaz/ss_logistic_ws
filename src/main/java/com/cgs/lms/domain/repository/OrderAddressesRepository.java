package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrderAddresses;

public interface OrderAddressesRepository extends JpaRepository<OrderAddresses, Integer> {

	List<OrderAddresses> findAllByOrderId(int id);

}
