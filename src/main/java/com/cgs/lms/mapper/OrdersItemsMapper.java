package com.cgs.lms.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.cgs.lms.domain.OrderItems;
import com.cgs.lms.dto.OrderItemsDto;

@Mapper(componentModel = "spring", uses = {})
public interface OrdersItemsMapper {

	OrderItemsDto orderItemToOrderItemDto(OrderItems order);

	List<OrderItemsDto> orderItemToOrdervDtoList(List<OrderItems> order);
}
